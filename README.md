<h1 align="center">EDUCALYTICS</h1>

## The Proposal

Developing an application capable of fetching data from an external source i.e. legacy application (called Skillshare), in order to supply users with enough information as to administer an educational institution.  

The data is produced through the regular use of a e-learning platform. Thus, a history of usage, including user interaction via chats and system logs, assignments, performance of students an user satisfaction are to be extracted, compiled and presented to our clients. 

Eventually, Educalytics should be able to convey the behaviour the legacy applications' users and provide the educational institution with enough intelligence, supporting its decision making process.

Following from these premises listed above, Educalytics will present an GUI by means of a DASHBOARD with the most important data from the educational institution vantage point.


## Project Description

In line with the Learning By Project methodology, our team (TECNOCODE) is working hard to deliver an operational application to a partner company called IONIC HEALTH, supported by our professors. Please see the details below.

For DASHBOARD (FRONTEND), please acess: https://gitlab.com/rafaelEstevam/front-educalytics

## Educalytics Project Status 


	SPRINT 01 - 19/SETEMBRO/2021 = OK; 
	SPRINT 02 - 10/OUTUBRO/2021 = OK;
	SPRINT 03 - 07/NOVEMBRO/2021 = OK;
	SPRINT 04 - 28/NOVEMBRO/2021 = OK;

## Features

- [x] 1) THE ARCHITECTURE OF EDUCALYTICS

![image](/uploads/e0032883a1c423499c2894d11e8499e6/image.png)

- [x]  2) FRONTEND;
- [x]  2.1) DASHBOARD (MVP);
![image](/uploads/517981e60e68b28b7e72d3d10446fa4a/image.png)

- [x]  2.2) LOGIN SCREEN (WITH CRIPTOGRAPHY "bcript");

<h1 align="center">
![image](/uploads/e0773cc284d37700a7e26a4b038c7b3d/image.png)
![image](/uploads/c1e112235e341cea25edc979989e9359/image.png)
</h1>

- [x]  2.3) REQUEST USING UNIQUE TOKEN;

![LOGIN-TOKEN-BCRIPT](/uploads/646571c981a760861dbb4c37d5311cf0/LOGIN-TOKEN-BCRIPT.gif)

- [x]  2.4) CONTINUOUS INTEGRATION;

![image](/uploads/34378882d78dcdd3253a0ef56a1f0b66/image.png)
![image](/uploads/94543a4b277168a2f502b2037cea70bb/image.png)
![image](/uploads/237fff34df8d7d61529b82d9b16b8eff/image.png)
![image](/uploads/1785d6f52205b99e41f9d10155a1b9f9/image.png)
![image](/uploads/e1b19eac58d659982df144302c1b99a2/image.png)
![image](/uploads/cb87d885f33c4105bf465036cc1de6ef/image.png)
![image](/uploads/b1cbf24ed91eecfc1fad66dd619c867e/image.png)

- [x]  2.5) STUDENT PARTICIPATION RESULTS BEING SHOWED VIA GRAPHICS ON DASHBOARD;
![image](https://i.ibb.co/7Vz4D3W/GRAFICO-DE-ENGAJAMENTO-1.png)
![image](https://i.ibb.co/925kHQh/GRAFICO-DE-ENGAJAMENTO-2.png)
![image](https://i.ibb.co/Br2WZqJ/GRAFICO-DE-ENGAJAMENTO-3.png)

- [x]  2.6) RESULTS ON EACH CLASS;

![image](https://i.ibb.co/D1HJPdb/PERFORMANCE-POR-DISCIPLINA.png)

- [x] 3) BACKEND - THE 8 LAYERS (MPConConRVSC).

Model, 
Payload, 
Config, 
Converter, 
Repository, 
Validator, 
Services, 
Controller.

![image](/uploads/0e674ebc3fe4fc49a8d5541a08872ad6/image.png)

![image](/uploads/c253e6519ed9b3e19c02988ad3fda2d9/image.png)

- [x] 3.1) BACKEND - VALIDATION (user and password).

![data security 01](/uploads/796c5457702781810a21ec2c57659593/WhatsApp-Video-2021-09-19-at-18.53.24.gif)

- [x] 3.2) BACKEND - DATABASE ACCESS (MVP for Sprint 01 = concluded).

![database access](/uploads/feb2ee0b86116211945623eaaaa58790/WhatsApp-Video-2021-09-19-at-18.53.36.gif)

- [x] 3.3) BACKEND - ETL.

![1](/uploads/21e823772a4943661033dcd66fb0d089/1.gif)
![2](/uploads/503a5796964bfe840c5c6a32118100e0/2.gif)
![3](/uploads/5a1b8a4ba4874ea3c8b52f8e854f3b07/3.gif)
![5](/uploads/e891dc30db9253bb2a73cd7980675054/5.gif)

- [x] 3.3.1) BACKEND - ETL TIME DIMENSION.

![image](/uploads/505e55286db7a593eb523cc84e898c2b/image.png)
![image](/uploads/f298a401ce83f50030e35abd0c758baa/image.png)

- [x] 3.3.2) BACKEND - ETL COURSE DIMENSION ON THE LEGACY APPLICATION.

![image](/uploads/f1b312c4656e2dc08aa6de984c31bc5a/image.png)
![image](/uploads/06662513e559dbc99ca404eaba33ce69/image.png)
![image](/uploads/3ebab2b710d5eeb5a0f065a6a5a88d9d/image.png)
![image](/uploads/a904e258f58fb0c7e434972f2e2c23f8/image.png)

- [x] 3.3.3) BACKEND - ETL COURSE TO DATAMART ON THE PROGRAMME DIMENSION OF THE LEGACY APPLICATION.

![image](/uploads/74509b0f9f1993f3fe64642bb2245bb8/image.png)
![image](/uploads/ffcc3ede8013d7d43c3a9031d20f2d8f/image.png)
![image](/uploads/fa0bc7f462673c0660de478abc6165f2/image.png)
![image](/uploads/b4a6f7dbe97abbbce304512ac8174021/image.png)
![image](/uploads/618157edc00a59dc9d342c3cf891a983/image.png)
![image](/uploads/c1194bc804482486775cb668dc582bad/image.png)

- [x] 4) EDUCALYTICS's DATABASE (RELATIONAL)

- [x] 4.1) LOGICAL MODEL

![image](/uploads/17d3c36f5538956ce7511dca92493aa3/image.png)

- [x] 4.2) ENTITY-RELATIONSHIP MODEL

![image](/uploads/503da79cc91e5b0b9c23ce3f66f0eaaa/image.png)

- [x] 4.3) EDUCALYTICS's DATABASE (RELATIONAL - MVP SPRINT 01)

![image](/uploads/13d93f79569bc96e400feae3baa779ec/image.png)

- [x] 4.4) EDUCALYTICS's DATABASE (RELATIONAL - MVP SPRINT 01)

![image](/uploads/1c0bc6a6ecc72ab2dbfef7997706bc14/image.png)

- [x] 4.5) SKILLSHARE DATABASE MODEL

![image](/uploads/d91d13b815360da6147c7a0d410849a4/image.png)

- [x] 4.6) DATAMART PERFORMANCE DATABASE MODEL

![image](/uploads/b0d01ac27de674663cb2c9eb39338b01/image.png)
![image](/uploads/5873d267c59f8cb8a58a11f81419e2f5/image.png)

- [x] 4.7) DATAMART PARTICIPATION ETL INTEGRATION

![image](https://i.ibb.co/3m6V6n6/DATAMART-ENGAJAMENTO.png)

- [x] 4.8) DATAMART PARTICIPATION - EXTRACTING DATA FROM MONGO DB

![image](https://i.ibb.co/ssS7yBm/EXTRACAO-MONGO-DB.png)

- [x] 4.9) DATAMART PARTICIPATION - EXTRACTING FROM DIMENSION TO FACT

![image](https://i.ibb.co/W27gGDW/EXTRACAO-DIMENSOES-FATO.png)

- [x] 5) EDUCALYTICS's DATABASE (NON-RELATIONAL)

![image](https://i.ibb.co/pXyJ1qQ/DATAMART-02-ENGAJAMENTO.png)

- [x] 5.1) DATAMART PARTICIPATION STRUCTURE

![image](https://i.ibb.co/b63mVjP/MODELAGEM-ATUALIZADA.png)

- [x] 5.1.1) DATAMART PARTICIPATION - CONNECTION COLLECTION

![image](https://i.ibb.co/kMLBtPv/COLLECTION-CONNECTIONS.png)

- [x] 5.1.2) DATAMART PARTICIPATION - MESSAGES COLLECTION

![image](https://i.ibb.co/NNj5jnf/COLLECTION-MESSAGES.png)

- [x] 5.2) NON-RELATIONAL DATABASE ETL

![image](https://i.ibb.co/wMrLpBB/ETL-BDNR.png)

- [x] 5.3) CHAT+LOGS REGISTERS INSERTED ON MONGO DB CLOUD
![image](https://i.ibb.co/WxhRFtm/DADOS-DO-CHAT-E-DOS-LOGS.png)

## Milestones

| Sprint | Data  | Video                                       |
|--------|-------|---------------------------------------------|
| 1      | September 19th, 2021 |---------------------------------------------|
| 2      | October 10th, 2021 |---------------------------------------------|
| 3      | November 7th, 2021 |---------------------------------------------|
| 4      | November 28th, 2021 |---------------------------------------------|




## Elicitation, Story Cards an Requirements
### The Problem we aim to solve

(translated to Portuguese).

O cliente busca obter indicadores e avaliar o uso e gerenciamento de sua plataforma de cursos, buscando os alunos que estão com boa frequência nas aulas, que entra em contato por chat para sanar dúvidas e avaliação das aulas dadas.

Com base nesse briefing, solicita uma plataforma funcional com capacidade analítica, com dashboards, histórico de chat e armazenamento desses dados. Para a realização da análise deverá ter especialmente:


* Ativação: Quantos usuários ativos (aluno/colaboradores) na plataforma legado;
* Engajamento: Conhecer o número de usuários, tipos, seu comportamento (matrícula, curso, disciplina, participação);
* Desempenho: qual o aproveitamento do aluno(nota atingida) e do professor (andamento da turma);
* Participação x taxas de conclusão x desempenho dos alunos/colaboradores;
* Avaliação de reação: quanto ao conteúdo apresentado, experiência do aluno/colaborador durante o curso;
* Registro do tempo de participação no curso;
* Guardar logs e histórico das conversas do chat;


## Requirements

#### Functional

(translated to Portuguese).

| Functional Requirements                                                 | Reference | Priority | Sprint |
|------------------------------------------------------------------------|--------|------------|--------|
| Dashboard: apresentar dados ao cliente (via backend e banco de dados 01 relacional)        | RF01   | 1          | 1      |
| Continuous Integration | RF02   | 1          | 4      |
| Tratar Logs| RF03   | 1          | 2      |
| Tratar Chats T   | RF04  | 1          | 3     |
| Acumular dados históricos tratados e gerar inteligência para o client, via DASHBOARD |RF05	| 1	| 4




#### Non-functional

(translated to Portuguese).

| Non-functional Requirements                            | Reference | 
|------------------------------------------------------|--------|
| Regras de segurança da informação         | RNF01  |
| Regras de privacidade              | RNF02  |
| Documentação | RNF03  |
| Escalabilidade  | RNF04  |
| Facilidade de Uso | RNF05 |



## User Stories
(translated to Portuguese).

| Quem? | O que? | Por quê? |
|-------|--------|----------|
| Aplicação Legado | Fornece plataforma de educação e gera dados históricos | Dados que serão tratados e analisados
| IONIC Health | Quer analisar dados históricos para a tomada de decisão | Para mapeamento de aproveitamento etc. dos cursos EAD |
| Tecnocode | Desenvolve a EDUCALYTICS | Para aplicar na prática os conhecimentos de programação, banco de dados (não)relacional, data warehouse, continuous inntegration,  além de adquirir experiência profissional em desenvolvimento de projetos |



## Usability:
(translated to Portuguese).

**HEURISTIC EVALUATION**

* Correspondência entre o sistema e o mundo real

O sistema possuirá uma interface intuitiva com menus e botões de ações de fácil entendimento, utilizando nomenclatura familiar aos seus usuários. As ações tais como acompanhamento dos indicadores por meio de Dashboard, chat por colaborador, avaliação por aula, induzindo os caminhos para que a utilização seja fluída.

* Controle do usuário e liberdade

Com o sistema intuitivo, o usuário de nível de gerenciamento possuirá uma certa liberdade no sistema, minimizando o número de cliques e de erros, pois conseguirá identificar claramente as funcionalidades e comandos disponíveis, sem precisar decorar procedimento algum. Sendo a facilidade em identificar cada gráfico proposto no dashboard, para fim de avaliar melhorias entre os alunos e colaboradores que realizam algum curso na plataforma.

* Design estético e minimalista

Com design intuitivo o sistema exibirá informações precisas e de fácil interpretação, com gráficos de cards, donuts e ícones familiares. 

## Diagrama de Casos de Uso
(translated to Portuguese).
![image](/uploads/6be9e0ac6a0faf868dde8634073d6984/image.png)

## Innovation
(translated to Portuguese).
Criamos o 1º BD a partir dos dados considerados necessários, a partir dos requisitos do cliente e da aplicação legado (Skillshare).  Subsequentemente, alteramos o protagonismo para as entidades que envolvem a ligação entre Alunos, Professores e Disciplina.
Então, estudamos quais ferramentas servirão aos requisitos (não) funcionais:


No FRONTEND - criptografia e token.
No BACKEND - arquitetura em oito camadas;
No BANCO DE DADOS 01 (RELACIONAL) - Primeira versão com todos os dados necessários (será decomposto);
No BANCO DE DADOS 02 (NÃO-RELACIONAL) - Receberá tabelas do BD01, segundo critérios de performance;
Distribuição dos BD - Se necessária a clusterização / fragmentação;
No DATA WAREHOUSE - Ferramentas de relatório, de ETL no terceiro banco de dados;



## Project Backlog

#### Sprint 01 
(translated to Portuguese).
## - MVP Dashboard;

- Reunião de kick-off com Ionic Health;

- Criação de repositório;

- Definição de cargos (SM e PO);

- Definição de nome para a aplicação;

- Brainstorm para discussão de primeiras ideias;

- Apresentação de proposta de aplicação;

- Criação do README.MD com informações sobre o projeto e instruções para execução da aplicação;

- Criação de modelo entidade relacionamento para definir como os dados serão estruturados;

- Início do Desenvolvimento do Core da Aplicação: Controller, Model (Arquitetura REST), Banco de Dados (MySQL); 

- Implantação das Camadas + Spring boot e demais entidades;

- Core da Aplicação Funcionando: Controller, Model (Arquitetura REST) e Banco de Dados (MySQL);

- CRUD: da 2ª Rota de submissão de requisições:  Alunos-matéria (via POSTMAN);

- Criação ou atualização automática das tabelas (Hibernate- MODEL);
   
- Validação de Dados: Campos que NÃO  podem estar vazios;

- Testes - Início de implementação de teste unitário de métodos;

#### Sprint 02

- Mapeamento das entidades que irão compor o Data Mart;

- Integração com o Pentaho;

- Pentaho extrai dados do BD da aplicação legado (Skillshare);

- Dados estão sendo enviados para o Data Mart Performance (banco de dados que compõe o Data Warehouse);

- Job salva os arquivos de instruções do ETL e arquiva em uma pasta;

- Inicio do DW: Para indicadores de alunos por matéria;

- Criação das entidades Student, Fact Performance, Course e Time com Controller, Model (Arquitetura REST) e Banco de Dados;

- Implementação de query dinâmica;

- Continuação da integração Back-end com FrontEnd + Dashboards para as novas entidades;

- Back-end acessa o Datamart Performance, entrega as novas informações desse banco de dados e disponibiliza para o dashboard (front-end);

- Dashboard interativo apresenta os novos dados;

#### Sprint 03.
- BD: Armazenando dados analítico do Dashboard;

- BD: População do BD com dados fictícios para teste de PERFORMANCE;

- Back-end: 2a API- para buscar mais indicadores referentes Alunos vs Professores Vs Matérias vs Cursos;

- Back-end: Finalização Integração FrontEnd + Dashboards;

- Back-end: Mapeamento para guardar logs do chat;

- Desenvolvimento de BD não relacional para armazenamento de chats e logs.

- Construção de banco de dados não relacional para armazenamento de informação de logs e chats

- Integração gráfica mostrando engajamento dos alunos, apresentando informações estatísticas de acesso a plataforma e trazendo informações de desempenho dos próprios alunos.

#### Sprint 04.

- Testes Unitários com Repository e Service;

- Testes de Integração com Selenium;

- Configuração do CI no Back-end nas branches Develop, Homolog e Master;

- Ajustes na estrutura do back-end;

- DW completo com os dados dos dashboards;

- Finalização do rotina de logs do chat;

- Testes automatizados;

- Versionamento de banco de dados;

#### Apresentação Final - dezembro de 2021.

- Bugs e Documentação;

#### Feira de Soluções - dezembro de 2021.


## Equipe:

#### Caroline Paz
[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/CarolPaz)
[![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/caroline-sousa-53a27972/)
RA: 1460281923049

#### Fábio Odaguiri
[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/ODAGAMMXIX)
[![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/fabioodaguiri/)
RA: 1460281923008

#### Felipe Santos
[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/felipefsc)
[![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/felipe-santos-454060187/)
RA: 1460281923011

#### Gabriela Momilli
[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/gabsmomilli)
[![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](linkedin.com/in/gabriela-momilli-105b1a184)
RA: 1460281923058

#### Rafael Oliveira
[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/rafaelEstevam)
[![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/rafael-estevam-de-oliveira/)
RA: 1460281823040

#### Wilson Vieira
[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/ZVIEWIL)
[![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/wilson-amore-vieira-filho-7a4420183)
RA: 1460281923041

## Running it up
Deployment
No seu dispositivo
Vide Pasta "DEPLOYMENT-FIND-STEPS-HERE" acima)

## Ferramentas Tecnologicas
💻

![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
![IntelliJ IDEA](https://img.shields.io/badge/IntelliJIDEA-000000.svg?style=for-the-badge&logo=intellij-idea&logoColor=white)
![VS CODE](https://img.shields.io/badge/IntelliJIDEA-000000.svg?style=for-the-badge&logo=intellij-idea&logoColor=white)
![Eclipse](https://img.shields.io/badge/Eclipse-FE7A16.svg?style=for-the-badge&logo=Eclipse&logoColor=white)
![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)
![MicrosoftSQLServer](https://img.shields.io/badge/Microsoft%20SQL%20Sever-CC2927?style=for-the-badge&logo=microsoft%20sql%20server&logoColor=white)
![MySQL](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white)
![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)
![Insomnia](https://img.shields.io/badge/Insomnia-black?style=for-the-badge&logo=insomnia&logoColor=5849BE)
![Postman](https://img.shields.io/badge/Postman-FF6C37?style=for-the-badge&logo=postman&logoColor=white)
![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)
![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)
![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white)
- Git Flow
- OBS Studio / Kdenlive
